# MyTree

### Requirements
Ubuntu >=20.04 with default Python distribution installed.

### Installation

With the bash script:
```bash
sudo chmod 774 install.sh
./install.sh
```

without the bash script:
```bash
chmod +x mytree.py
cp -r mytree.py mytree
mkdir -p ~/bin/mytree
cp -r . ~/bin/mytree
export PATH=$PATH":$HOME/bin/mytree"
```

### Usage

`mytree [-j | --json, -t | --time]`

* `--json` to output in json format
* `--time` tp show files more recent than a date in ISO 8601 format. Folder struct is shown

### Reference
* [How Do I Make My Own Command-Line Commands Using Python?](https://dbader.org/blog/how-to-make-command-line-commands-with-python) to review the correct use of Linux's `shebang`.
* [Argparse documentation](https://docs.python.org/3/library/argparse.html)
* [Capture console log](https://stackoverflow.com/questions/33767627/python-write-unittest-for-console-print)
* [JSON output format](https://askubuntu.com/questions/1053065/output-disc-space-of-folders-as-json)