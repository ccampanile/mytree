import os
import sys, io
import unittest
sys.path.insert(0, os.path.join(os.path.dirname(os.path.realpath(__file__)),'..'))
from app import MyTree

class MyTreeTest(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        super(MyTreeTest, self).setUpClass()
        self.root = os.path.join(os.path.dirname(os.path.realpath(__file__)),'folders')
        self.results = os.path.join(os.path.dirname(os.path.realpath(__file__)),'results')
        

    def test_case1(self):
        """Folder with one file"""
        mytree = MyTree(os.path.join(self.root,'case_1'))

        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput
        
        mytree.display()

        sys.stdout = sys.__stdout__
        captured = capturedOutput.getvalue()

        with open(os.path.join(self.results, 'case_1'), 'r') as res_file:
            res = res_file.read()

        self.assertEqual(captured, res)

    def test_case1json(self):
        """Folder with one file"""
        mytree = MyTree(os.path.join(self.root,'case_1'))

        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput
        
        mytree.display(as_json=True)

        sys.stdout = sys.__stdout__
        captured = capturedOutput.getvalue()

        with open(os.path.join(self.results, 'case_1_json'), 'r') as res_file:
            res = res_file.read()

        self.assertEqual(captured, res)

    def test_case2(self):
        """Empty folder"""
        mytree = MyTree(os.path.join(self.root,'case_2'))
        
        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput
        
        mytree.display()

        sys.stdout = sys.__stdout__
        captured = capturedOutput.getvalue()

        with open(os.path.join(self.results, 'case_2'), 'r') as res_file:
            res = res_file.read()

        self.assertEqual(captured, res)


    def test_case2json(self):
        """Empty folder"""
        mytree = MyTree(os.path.join(self.root,'case_2'))
        
        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput
        
        mytree.display(as_json=True)

        sys.stdout = sys.__stdout__
        captured = capturedOutput.getvalue()

        with open(os.path.join(self.results, 'case_2_json'), 'r') as res_file:
            res = res_file.read()

        self.assertEqual(captured, res)
    
    def test_case3(self):
        """Tree Structure"""
        mytree = MyTree(os.path.join(self.root,'case_3'))
        
        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput
        
        mytree.display()

        sys.stdout = sys.__stdout__
        captured = capturedOutput.getvalue()

        with open(os.path.join(self.results, 'case_3'), 'r') as res_file:
            res = res_file.read()

        self.assertEqual(captured, res)
    
    def test_case3json(self):
        """Tree Structure - as json"""
        mytree = MyTree(os.path.join(self.root,'case_3')) # same input as case 3
        
        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput
        
        mytree.display(as_json=True)

        sys.stdout = sys.__stdout__
        captured = capturedOutput.getvalue()

        with open(os.path.join(self.results, 'case_3_json'), 'r') as res_file:
            res = res_file.read()

        self.assertEqual(captured, res)

    def test_case4(self):
        """Not a folder - assertion error"""
        def gen_error():
            mytree = MyTree(os.path.join(self.root,'case_4', 'file_1'))
        
        self.assertRaises(AssertionError, gen_error)

        

if __name__=='__main__':
    unittest.main()