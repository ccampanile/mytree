import os
import re
from collections import Counter
from typing import List
from pprint import pprint
import time
from datetime import datetime

class Marker:
    T='├──'
    L='└──'
    I='│  '
    E='   '


class MyTree:

    def __init__(self, root:str, moment:str=None) -> None:
        """ Represents a folder tree-structure.

        Parameters
        ----------
        root: str
            root folder
        
        moment: str
            ISO 8601 datetime. Filter more recent items
        """
        assert os.path.isdir(root), "root is not a folder"
        self.root=root
        self.report=Counter()
        self.moment=moment
        self.contents=self._drilldown()

    def add_file(self):
        self.report['files']+=1

    def add_directory(self):
        self.report['directories']+=1
    
    def display_report(self):
        print(f'\n{self.report["directories"]} directories, {self.report["files"]} files')

    def _drilldown(self, root:str=None) -> List[dict]:
        """ Retrieve folder tree structure as dictionary. Format:
        [
            {
                'name':'<file_name>'
            },
            {   
                'name':'<folder_name>',
                'contents':[
                    {
                        'name':'<file_name>'
                    }
                ]
            },
        ]

        Parameters
        ----------
        root:str
            root folder absolute path

        Returns
        -------
        contents: list
        """
        if root is None:
            root = self.root
        contents = []
        for item in sorted(os.listdir(root), key=str.lower):
            path = os.path.join(root, item)
            # folder -> preorder transversal
            if os.path.isdir(path):
                subcontents = self._drilldown(root=path)
                node = {
                    "type":"directory",
                    "name":item,
                    "contents": subcontents
                }
                self.add_directory()
            else:
                if self.is_older_than(path):
                    continue
                node = {
                    "type":"file",
                    "name":item
                }
                self.add_file()
            contents.append(node)
        return contents


    def display(self, as_json=False) -> None:
        """ Display 
        """

        def display_subtree(root, contents, prefix:str='.') -> None:
            if prefix=='.': 
                print(prefix)
                prefix=''

            for i,item in enumerate(contents):
                name = item.get('name')
                marker = Marker.L if i==(len(contents)-1) else Marker.T
                if item.get('type')=='directory':
                    print(prefix + marker + name)
                    additional_prefix = Marker.E if i==(len(contents)-1) else Marker.I
                    display_subtree(
                        os.path.join(root,name), 
                        item.get('contents'), 
                        prefix=prefix+additional_prefix)
                else:
                    print(prefix + marker + name)
        
        if as_json:
            to_display = [
                    {
                        "type":"directory",
                        "name":".",
                        "contents": self.contents.copy()
                    },
                    {
                        "type": "report", 
                        "files": 0, 
                        "directories": 0,
                        **self.report
                    }
                ]
            pprint(to_display)
        else:
            display_subtree(self.root, self.contents)
            self.display_report()


    def is_older_than(self, path):
        """ Check if last modification of file occurred later than a date

        Parameters
        ----------
        path:str
            filepath to check
        """
        if self.moment:
            ref_time = datetime.fromisoformat(self.moment.replace('Z', '+00:00')).timestamp() 
            
            modTimesinceEpoc = os.path.getmtime(path) 
            file_time = time.mktime(time.localtime(modTimesinceEpoc)) # does not catch local time

            return ref_time > file_time 
            
        return False


