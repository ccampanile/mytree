#!/usr/bin/env python3
import argparse, os
from app import MyTree


if __name__=='__main__':
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('-h', '--help', action='help', default=argparse.SUPPRESS,
                    help='mytree base use: mytree\n')
    parser.add_argument('-j', '--json', action="store_true", help="output in json format")
    parser.add_argument('-t', '--time', help="show files more recent than a date in ISO 8601 format. Folder struct is shown")
    args = parser.parse_args()

    mytree=MyTree(root=os.getcwd(), moment=args.time) # "2021-04-24T19:50:00.000000Z"
    mytree.display(as_json=args.json)
